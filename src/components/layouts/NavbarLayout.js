import React,{useState}  from 'react';
import { Navbar, Nav, Image, NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";
import changlang from "../pages/changelang/changlang"

export const NavbarLayout = () => {
    const [language, setLanguage] = useState("en");

const onChangeLanguage = (str) => {
        changlang.setLanguage(str);
        setLanguage(str);
      }
      
    return (
        <div>
            <Navbar bg="light" expand="lg">
            <Navbar.Brand as={Link} to="/" href="#home"><Image src="/images/KSHRD_logo.png" width="50" alt="kshrd_logo"/> {changlang.title_managerment}</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link as={Link} to="/articles" href="#home">{changlang.acticle}</Nav.Link>
                        <Nav.Link as={Link} to="/category" href="#link">{changlang.category}</Nav.Link>
                        <NavDropdown title={changlang.language} id="basic-nav-dropdown">
                        <NavDropdown.Item onClick={() => onChangeLanguage("en")}>{changlang.english}</NavDropdown.Item>
                        <NavDropdown.Item onClick={() => onChangeLanguage("kh")}>{changlang.khmer}</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    
                </Navbar.Collapse> 
            </Navbar>
        </div>
    )
}

export default NavbarLayout;