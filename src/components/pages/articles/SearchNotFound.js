import React from 'react'
import '../../../styles/searchNotFound.css'

export default function SearchNotFound() {
    return (
        <div style={{textAlign: 'center', marginTop: '100px'}}>
            <div  class="loading">Search Not Found.</div>
            <div style={{marginLeft: '10px'}} class="circle"></div>
            <div style={{marginLeft: '20px'}} class="circle"></div>
            <div style={{marginLeft: '30px'}} class="circle"></div>
        </div>
    )
}
