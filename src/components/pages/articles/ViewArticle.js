import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Container, Row, Col, Image } from 'react-bootstrap';
import { getArticleById } from '../../../redux/actions/articleActions';
import { BoxLoading } from 'react-loadingg';

class ViewArticle extends Component {
    componentDidMount() {
        const { getArticleById, match } = this.props;
        const id = match.params.id;
        if(id) {
            getArticleById(match.params.id, (res) => res.data.data);
        }
    }

    render() {
        const { current, loading } = this.props.article;
        if(loading || current === null) {
            return <BoxLoading />;
        }
        return (
            <div>
            <Container className="my-5">
                <Row>
                    <Col md={4} className="mb-3">
                        <Image src={!current.image ? "/images/default-img.png" : current.image} className="img-fluid" rounded />
                    </Col>
                    <Col md={8}>
                        <h2>{current.title}</h2>
                        <p>Create Date : {current.createdAt}</p>
                        <p className= "mb-4">Category : { current.category ? current.category.name : "No Type" }</p>
                        <p>{current.description}</p>
                    </Col>
                </Row> 
            </Container>
        </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        article: state.articleReducer
    }
}

export default connect(mapStateToProps, { getArticleById })(ViewArticle);