import React, { useState, useEffect } from 'react'
import { Form, Button, Container, Row, Col, Image } from "react-bootstrap";
import { connect } from "react-redux";
import { getArticleById, updateArticle } from "../../../redux/actions/articleActions";
import { getAllCategories } from '../../../redux/actions/categoryActions';
import Axios from 'axios';
import { BoxLoading } from 'react-loadingg';

const UpdateArticle = ({ match, getArticleById, updateArticle }) => { 
    const [loading, setLoading] = useState(false);
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [category, setCategory] = useState("");
    const [categoryId, setCategoryId] = useState("");
    const [image, setImage] = useState("");
    const [imagePreviewUrl, setImagePreviewUrl] = useState("");
    const [categories, setCategories] = useState([]);
    const [titleError, setTitleError] = useState("");
    const [descriptionError, setDescriptionError] = useState("");
    
    useEffect(() => {
        getArticleById(match.params.id, (res) => {
            if(res.status === 200) {
                const current = res.data.data;
                setTitle(current.title);
                setCategoryId(current.category ? current.category._id : "")
                setCategory(current.category ? current.category.name : "")
                setDescription(current.description);
                setImagePreviewUrl(current.image);
            }
        });
    }, [getArticleById, match.params.id])

    useEffect(() => {
        getAllCategories((res) => { 
            if(res.status === 200) {
                setCategories(res.data.data);
            } 
        })
    }, [])

    const validate = () => {
        let titleError = "";
        let descriptionError = "";
        if(title === "") {
            titleError = "Please input title!";
        }
        if(description === "") {
            descriptionError = "Please input description!";
        }
        if(titleError || descriptionError) {
            setTitleError(titleError);
            setDescriptionError(descriptionError);
            return false;
        }
        return true;
    }

    const onSubmit = (event) => {
        event.preventDefault();
        const isValid = validate();
        if(isValid) {
            const newArticle = {}; 
            newArticle._id = match.params.id;
            newArticle.title = title;
            newArticle.description = description;
            newArticle.category = {
                "_id": categoryId,
            };
            if(image) {
                let fd = new FormData();
                fd.append('image', image, image.name);
                setLoading(true);
                //post image
                Axios.post("http://110.74.194.125:3535/api/images", fd, { 
                    headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }})
                    .then(res => {
                        if(res.status === 200) {
                            newArticle.image = res.data.url;
                            updateArticle(newArticle, (res) => {
                                if(res.status === 200) {
                                    setLoading(false);
                                    setTitle("");
                                    setDescription("");
                                    setCategoryId("");
                                    setCategory("");
                                    setImage("");
                                    setImagePreviewUrl("");
                                    alert(res.data.message);
                                    window.history.back();
                                }
                            });
                        }
                    })
                    .catch(error => console.log(error.response));
                }
            else {
                newArticle.image = imagePreviewUrl;
                setLoading(true);
                updateArticle(newArticle, (res) => {
                    if(res.status === 200) {
                        setLoading(false);
                        setTitle("");
                        setDescription("");
                        setCategoryId("");
                        setCategory("");
                        setImage("");
                        setImagePreviewUrl("");
                        alert(res.data.message);
                        window.history.back(); 
                    }
                });
            }
        }
    }

    const onCancel = (event) => {
        event.preventDefault();
        window.history.back();
    }

    if(loading || categories.length === 0) {
        return <BoxLoading />;
    }
    return (
        <div>
            <Container className="py-3">
                <h1>Update Article</h1>
                <Row>
                    <Col md={7}>
                        <Form>
                            <Form.Group>
                            <Form.Label>Title</Form.Label>
                            <Form.Control type="text" 
                                placeholder="Input title" 
                                name="title" 
                                onChange={(event) => setTitle(event.target.value)}
                                value={title}/>
                            <h5 className="text-danger">{ titleError ? titleError : ""}</h5>
                            </Form.Group>
                            <Container>
                             <Row>
                                <div className="form-group">
                                    <Form.Label className="mr-3">Category: </Form.Label>
                                    { categories.map(c => 
                                        <div className="form-check form-check-inline" key={c._id}>
                                            <input
                                                type="radio"
                                                className="form-check-input"
                                                name="category"
                                                value={c.name}
                                                id={c._id}
                                                checked={category === c.name}
                                                onChange={(event) => {
                                                    setCategoryId(event.target.id); 
                                                    setCategory(event.target.value); 
                                                }}
                                            />
                                            <label className="form-check-label">{c.name}</label>
                                        </div>
                                    )}
                                    </div>         
                                </Row>
                            </Container>
                            <Form.Group>
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" 
                                    placeholder="Input description" 
                                    name="description" 
                                    onChange={(event) => setDescription(event.target.value)}
                                    value={description}/>
                                <h5 className="text-danger">{ descriptionError ? descriptionError : ""}</h5>
                            </Form.Group>
                            <Form.File 
                                type="file"
                                id="custom-file"
                                label="Custom file input"
                                name="image"
                                custom
                                className="mb-3"
                                onChange={(event) => {
                                    let reader = new FileReader();
                                    let file = event.target.files[0];
                                    reader.onloadend = () => {
                                        setImage(file);
                                        setImagePreviewUrl(reader.result)
                                    }
                                    reader.readAsDataURL(file);
                                }}
                            />
                            <Button className="mr-1" variant="dark" type="submit" onClick={onSubmit}>Update</Button>
                            <Button variant="primary" type="submit" onClick={onCancel}>Cancel</Button>
                        </Form>
                    </Col>
                    <Col md={5}>
                    { imagePreviewUrl ? <Image src={imagePreviewUrl} style= {{width: '100%'}} rounded /> :  
                    <Image style= {{width: '100%'}} src="/images/default-img.png" rounded />
                    }
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default connect(null, { getArticleById, updateArticle })(UpdateArticle);

