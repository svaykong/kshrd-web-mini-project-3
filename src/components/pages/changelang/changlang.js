import LocalizedStrings from 'localized-strings';
let changlang = new LocalizedStrings(
  {
    
    kh: {
      acticle: "អត្ថបទ",
      category: "ប្រភេទ",
      title_managerment: "ប្រព័ន្ធគ្រប់គ្រងពត័មាន",
      language: "ភាសារ",
      khmer:"ខ្មែរ",
      english:"អង់គ្លេស",
      name:"ឈ្មោះ",
      action:"សកម្នភាព",
      add_article:"បន្ថែមអត្ថបទ",
      search:"ស្វែងរក",
      create_date:"កាលបរិច្ឆេទ",
      view:"មើល",
      delete:"លុប",
      edit:"កែប្រែ",
      title:"ចំណង ជើង",
      description:"ការពិពណ៍នា",
      thumbnail:"រូបតំណាង"
    },
    en: {
      acticle: "Acticle",
      category: "Category",
      title_managerment: "Article Managerment",
      language: "Language",
      khmer:"Khmer",
      english:"Enlish",
      name:"Name",
      action:"Action",
      add_article:"ADD ARTICLE",
      search:"Search",
      create_date:"Create Date",
      view:"View",
      delete:"Delete",
      edit:"Edit",
      title:"Title",
      description:"Description",
      thumbnail:"Thumbnail"
    },
  },
  
 
);
console.log(changlang.name);
export default changlang;


